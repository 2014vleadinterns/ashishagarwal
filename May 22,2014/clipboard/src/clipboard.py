import sys

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset", "getsize"]

__text__ = None
__blob__ = None
__data__ = None
__textsize__ = 50
__blobsize__ = 10000000
__flagtext__ = 0
__flagblob__ = 0

def copytext(text):
    global __text__
    global __size__
    if len(text) <= __textsize__:
        __text__ = text
        #print __text__
        __size__ = len(text)
        __flagtext__ = 0
        return __flagtext__         
    else:
        __flagtext__ = 1
        copy_to_file(text)
        return __flagtext__

def copyblob(blob):
    global __blob__
    global __size__
    if len(blob) <= __blobsize__:
        __blob__ = blob
        __size__ = len(blob)
        __flagblob__ = 0
        return __flagblob__
    else:
        __flagblob__ = 1
        copy_to_file(blob)
        return __flagblob__

def gettext():
    global __text__
    global __data__
    #print __text__
    if __flagtext__ == 1:
        __data__ = read_file()
        return __data__
    else:
        return __text__
        

def getblob():
    global __blob__
    global __data__
    if __flagblob__ == 1:
        __data__ = read_file()
        return __data__
    else:
        return __blob__

def getsize():
    global __size__
    return __size__

def reset():
    global __text__, __blob__, __
    __text__ = None
    __blob__ = None
    __size__ = None

def copy_to_file(data):
    try:
        fh = open("testfile", "ab+")
        fh.write(data)
        fh.write("\n\n")
    except IOError:
        print "Error: can\'t find file or read data"
    else:
        #print "Written content in the file successfully"
        fh.close()

        #f = tempfile.TemporaryFile()
        #f.write(data)
        #f.seek(0) 
        #print f.read() 
        #f.close() 

def read_file():
    try:
        fh = open("testfile", "r")
        global __data__
        __data__ = fh.read()
        print __data__
        return __data__
    except IOError:
        print "Error: can\'t find file or read data"
    else:
        print "Read content in the file successfully"
        fh.close()

##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
