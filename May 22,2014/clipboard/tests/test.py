# -*- coding: utf-8 -*-
import clipboard
import Image
import base64
import tempfile

__defsize__ = 20
__defimg__ = 1000000

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
        print "Initial State: Test and Blob both empty...!!!"
    
    def test_reset_clipboard():
        clipboard.reset()
        assert(clipboard.gettext() == None)
        assert(clipboard.getblob() == None)
        print "Reset State: Test and Blob forced to empty...!!!"
    
    def test_copy_hindi_text():
        clipboard.reset()
        msg = 'विकिपीडिया:इण्टरनेट पर हिन्दी के साधन'
        print len(msg)
        flag = clipboard.copytext(msg)
        if flag == 0:   
            text = clipboard.gettext()
            assert(msg == text)
            assert(clipboard.getblob() == None)
            print "Text State: Hindi Content copied to clipboard...!!!"
        elif flag == 1:                
            print "Text State: Size to copy is more than buffer...!!!"
            print "Hindi Content will now copied to Temperory File now...!!!"
            #test_open_file(msg)
            assert(clipboard.getblob() == None)
            
    def test_copy_english_text():
        clipboard.reset()
        msg = "Hello World !!"
        flag = clipboard.copytext(msg)
        #size = clipboard.getsize()
        if flag == 0:
            text = clipboard.gettext()
            assert(msg == text)
            print "Text State: English Content copied to clipboard...!!!"
            assert(clipboard.getblob() == None)
        elif flag == 1:                
            print "Text State: Size to copy is more than buffer...!!!"
            print "English Content will now copied to Temperory File now...!!!"
            #test_open_file(msg)
            assert(clipboard.getblob() == None)

    def test_copy_blob():
        clipboard.reset()
        with open("url.jpeg", "rb") as imageFile:
             msg = base64.b64encode(imageFile.read())
        flag = clipboard.copyblob(msg)
        if flag == 0:
	    blob = clipboard.getblob()
            assert(msg == blob)
            print "Blob State: Content copied to clipboard...!!!"
            assert(clipboard.gettext() == None)
        elif flag == 1:                
            print "Blob State: Size to copy is more than buffer...!!!"
            print "Content will now copied to Temperory File now...!!!"
            #test_open_file(msg)
            assert(clipboard.gettext() == None)            
        
    def test_copy_both():
        clipboard.reset()
        msg1 = "Hello World !!"
        with open("url.jpeg", "rb") as imageFile:
             msg2 = base64.b64encode(imageFile.read())
        flag1 = clipboard.copytext(msg1)
	flag2 = clipboard.copyblob(msg2)
	if flag1 == 0 and flag2 == 0:
	    text = clipboard.gettext()
            blob = clipboard.getblob()
            assert(msg1 == text)
            assert(msg2 == blob)
            print "Text & Blob State: Both content copied to clipboard...!!!"
        elif flag1 == 0 and flag2 == 1:
            text = clipboard.gettext()
            assert(msg1 == text)
            print "Text & Blob State: Text copied to clipboard...!!!"
            print "                 : Blob Size to copy is more than buffer...!!!"
            print "                 : Blob Content will now copied to Temperory File now...!!!"
            #test_open_file(msg2) 
        elif flag1 == 1 and flag2 == 0:
            blob = clipboard.getblob()
            assert(msg2 == blob)
            print "Text & Blob State: Blob copied to clipboard...!!!"
            print "                 : Text Size to copy is more than buffer...!!!"
            print "                 : Text Content will now copied to Temperory File now...!!!"
            #test_open_file(msg1)
        else:
            print "Text & Blob State: Size to copy is more than buffer...!!!"
            print "Content will now copied to Temperory File now...!!!"
            #test_open_file(msg1)
            #test_open_file(msg2)             
        
    def test_copy_text_blob():
        text = clipboard.gettext()
        print text
        if text == None:
            print text 
            test_copy_blob() 
        else:
            with open("url.jpeg", "rb") as imageFile:
                msg = base64.b64encode(imageFile.read())
            flag = clipboard.copyblob(msg)
            if flag == 0:
                blob = clipboard.getblob()
                assert(msg == blob)
                assert(clipboard.gettext() == text)
                print "Blob State: Content copied to clipboard...!!!"      
            elif flag == 1:                
                print "Blob State: Size to copy is more than buffer...!!!"
                print "Content will now copied to Temperory File now...!!!"
                test_open_file(msg)
                assert(clipboard.gettext() == text)
                                     
    def test_copy_blob_text():
        blob = clipboard.getblob()
        print len(blob)
        if blob == None:
            #print text 
            test_copy_text() 
        else:
            msg = "Hello World !!"
            flag = clipboard.copytext(msg)
            if flag == 0:
                text = clipboard.gettext()
                assert(msg == text)
                print "Text State: English Content copied to clipboard...!!!"
                assert(clipboard.getblob() == blob)
            elif flag == 1:                
                print "Text State: Size to copy is more than buffer...!!!"
                print "English Content will now copied to Temperory File now...!!!"
                assert(clipboard.getblob() == blob)
    
    def test_copy_text_text():
        text = clipboard.gettext()
        blob = clipboard.getblob()
        print "Old Text :", text
        clipboard.copytext("Ashish")
        text = clipboard.gettext()
        assert(text == "Ashish")
        print "New Text :", text
        assert(clipboard.getblob() == blob)

    def test_copy_blob_blob():
        text = clipboard.gettext()
        blob = clipboard.getblob()
        print "Old Blob :", len(blob)
        with open("url1.jpeg", "rb") as imageFile:
            msg = base64.b64encode(imageFile.read())
        clipboard.copyblob(msg)
        blob = clipboard.getblob()        
        assert(msg == blob)
        print "New Blob :", len(blob)
        assert(clipboard.gettext() == text)

    #def test_open_file(data):
    #    try:
    #        fh = open("testfile", "wb+")
    #        fh.write(data)
    #    except IOError:
    #        print "Error: can\'t find file or read data"
    #    else:
    #        print "Written content in the file successfully"
    #        fh.close()

        #f = tempfile.TemporaryFile()
        #f.write(data)
        #f.seek(0) 
        #print f.read() 
        #f.close() 
 
   
    test_empty_clipboard()
    test_reset_clipboard()
    test_copy_english_text()
    test_copy_hindi_text()
    test_copy_blob()
    test_copy_both()
    test_copy_text_blob()
    test_copy_blob_text()
    test_copy_text_text()
    test_copy_blob_blob()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard.reset()
        clipboard.addobserver(anobserver)
        #clipboard.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
