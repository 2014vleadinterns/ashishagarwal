# My first python program.
# Ashish Agarwal
# May 20,2014

import sys

def greet(name):
    print "Hello", name

def main(args):
    print args
    for i in args:
        greet(i)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "usage: greet <name>"
    else:
        main(sys.argv)


