import re
import xml.etree.cElementTree as ET

__list__ = []

#def read_file():
f = open('input')
for line in iter(f):
    print line
    s = line.split()
    print s
    __list__.append(s)
print __list__
f.close()

root = ET.Element("Instruction_Set")
for x in __list__:
    doc = ET.SubElement(root, "Instruction")

    field5 = ET.SubElement(doc, "Input")
    field5.text = x[0] + x[1] + x[2] + x[3]

    field1 = ET.SubElement(doc, "Operator")
    if x[0] == '+' or x[0] == '-' or x[0] == '*' or x[0] == '/' or x[0] == '<<' or x[0] == '>>' or x[0] == '~':
         field1.text = x[0] + ' is Valid Operator'
    else:
         field1.text = x[0] + ' is Invalid Operator'

    field2 = ET.SubElement(doc, "Operand-1")
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', x[1]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', x[1]) or re.match('([-][0-9]*$)', x[1]) or re.match('[0-9]*$',x[1]):
         field2.text = x[1] + ' is Valid Operator'
    else:
         field2.text = x[1] + ' is Invalid Operator'

    field3 = ET.SubElement(doc, "Operand-2")
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', x[2]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', x[2]) or re.match('([-][0-9]*$)', x[2]) or re.match('[0-9]*$',x[2]):
         field3.text = x[2] + ' is Invalid Operator'
    else:
         field3.text = x[2] + ' is Valid Operator'
    
    field4 = ET.SubElement(doc, "Operand-3")
    if re.match('([_]*[a-zA-Z][a-zA-Z0-9_]*$)', x[3]) or re.match('([a-zA-Z][a-zA-Z0-9_]*$)', x[3]):
         field4.text = x[3] + ' is Invalid Operator'
    else:
         field4.text = x[3] + ' is Valid Operator'

tree = ET.ElementTree(root)
tree.write("Instruction.xml")    
