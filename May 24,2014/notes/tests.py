from notes import *

def run_note_tests():
    def test_reset_notes():
    	reset_notes()
    	assert(get_notes()==[])
    
    def test_new_note():
    	new_note("Ashish", "Good Morning")  
    	new_note("Ash", "Night")
    	new_note("Ashu", "Good Evening")
    	new_note("Bittu", "Good Ashish")
    	assert(get_notes()==[("Ashish", "Good Morning"), ("Ash", "Night"), ("Ashu", "Good Evening"), ("Bittu", "Good Ashish")])
        print "Initial Notes: ", get_notes()
    
    def test_find_notes():
        find = find_notes("ashish")
    	print "Notes finded: ", find
    
    def test_delete_node_by_title():
    	delete_node_by_title("tt")
        print "Notes after Deletion: ", get_notes()

    def test_delete_node_by_key():
        delete_node_by_key("Night")
        print "Notes after Deletion: ", get_notes()
        
    def test_edit_note():
        find = find_notes("Morning")
        if find:
            new_note("qwerty", "asdfgh")
            delete_node_by_key("Morning")
            print "Notes after Editing: ", get_notes()

    def test_save_note():
        save_note()

            

    test_reset_notes()
    test_new_note()
    test_find_notes()
    test_delete_node_by_title()
    test_delete_node_by_key()
    test_edit_note() 			
    test_save_note()    

run_note_tests()
		

