import re
from xml.dom.minidom import parseString
import xml.etree.cElementTree as ET

__notes__ = []
__find__ = []

def new_note(title,body):
    global __notes__
    if title == None:
	title = "Untitled Note"
    note = (title,body)
    __notes__.append(note)    

def find_notes(key):
    global __notes__
    find = []
    if key == None:
	key = "Untitled Key"
    for x in __notes__:
        if re.search(key, x[0], re.IGNORECASE) or re.search(key, x[1], re.IGNORECASE):
            find.append([x])
    return find
	    
def delete_node_by_title(title):
    global __notes__
    if title==None:
        title="Untitled"
    for x in __notes__:
        if re.search(title, x[0], re.IGNORECASE):
            delete = [x]
            __notes__.remove(delete[0])

def delete_node_by_key(key):
    global __notes__
    if key==None:
        key="Untitled"
    for x in __notes__:
        if re.search(key, x[0], re.IGNORECASE) or re.search(key, x[1], re.IGNORECASE):
            delete = [x]
            __notes__.remove(delete[0])

def save_note():  
    # create a new document
    #doc = parseString('<Master Note/>'.encode('UTF-8'))
    #art = doc.documentElement
    # create a sect1 element
    #s1 = doc.createElementNS(None,'Note')
    # add it under the root element
    #art.appendChild(s1)
    # create a title element with a text node inside
    #s1.appendChild(doc.createElementNS(None,'title'))
    #title = doc.createTextNode('Introduction to XML')
    #s1.firstChild.appendChild(title)
    #s1.appendChild(doc.createElementNS(None,'content'))
    #txt = doc.createTextNode('WRITE ME!')
    #s1.lastChild.appendChild(txt)
    # write the result
    #print doc.toprettyxml()
    
    root = ET.Element("Master_Note")
    for x in __notes__:
        doc = ET.SubElement(root, "Note")
        field1 = ET.SubElement(doc, "Title")
    #field1.set("name", "blah")
        field1.text = x[0]
        field2 = ET.SubElement(doc, "Content")
    #field2.set("name", "asdfasd")
        field2.text = x[1]
    tree = ET.ElementTree(root)
    tree.write("Notes.xml")
    
def get_notes():
    global __notes__
    return __notes__

def reset_notes():
    global __notes__
    __notes__=[] 
